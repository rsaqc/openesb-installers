#!/bin/bash -x

################################################################################
# get the path to the current directory and change to it

SCRIPT_DIR=`dirname $0`
cd ${SCRIPT_DIR}

################################################################################
# some defaults
if [ -z "$WORKSPACE" ] ; then 
   if [ "." = "$SCRIPT_DIR" ] ; then
       # like running "sh build.sh" or ./build.sh
       if [ -n "$CYGWIN" ] ; then
           WORKSPACE=`pwd | sed "s/\/cygdrive\/\(.\)/\1:/g"`
       else
           WORKSPACE=`pwd`
       fi
   else
       WORKSPACE="$SCRIPT_DIR"
   fi
fi

[ -z "$BUILD_NUMBER" ] && BUILD_NUMBER="0"
[ -z "$BUILD_DATE" ] && BUILD_DATE="00000000000000"

################################################################################
# load the properties
. ./build-private.sh

################################################################################
# define the environment for running ant
export ANT_OPTS

################################################################################
# run the build

ant build \
        \"-Dinstallers.build.number=${BUILD_NUMBER}\" \
        \"-Dinstallers.build.date=${BUILD_DATE}\" \
        \"-Doutput.dir=${OUTPUT_DIR}\" \
        \"-Dcache.dir=${CACHE_DIR}\" \
        \"-Djdk.home=${JAVA_HOME}\" \
        \"-Dglassfish.download.site=${GLASSFISH_DOWNLOAD_SITE}\" \
        \"-Dopenesb-core.download.site=${OPENESB_CORE_DOWNLOAD_SITE}\" \
        \"-Dopenesb-components.download.site=${OPENESB_COMPONENTS_DOWNLOAD_SITE}\" \
        \"-Dopenesb-glassfish-addons.download.site=${OPENESB_GLASSFISH_ADDONS_DOWNLOAD_SITE}\" \
        \"-Dopenesb-ide.download.site=${OPENESB_IDE_DOWNLOAD_SITE}\" \
        ${ADDITIONAL_PARAMETERS} \
        $* 2>&1

ERROR_CODE=$?
if [ $ERROR_CODE != 0 ]; then
    echo "ERROR: $ERROR_CODE -- installers build failed"
    exit $ERROR_CODE
fi

chmod +x $OUTPUT_DIR/*.sh

################################################################################

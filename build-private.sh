#!/bin/bash -x

OUTPUT_DIR=$WORKSPACE/dist
mkdir -p $OUTPUT_DIR
CACHE_DIR=$WORKSPACE/cache
mkdir -p $CACHE_DIR

ANT_OPTS="-Xmx2048m -Djavac.target=1.6 -Djavac.source=1.6"

GLASSFISH_DOWNLOAD_SITE=http://dlc.sun.com.edgesuite.net/javaee5/v2.1.1_branch/promoted

OPENESB_CORE_DOWNLOAD_SITE=http://build2.open-esb.net:8080/jenkins/job/release-openesb-core/
OPENESB_COMPONENTS_DOWNLOAD_SITE=http://build2.open-esb.net:8080/jenkins/job/release-openesb-components/
OPENESB_GLASSFISH_ADDONS_DOWNLOAD_SITE=http://build2.open-esb.net:8080/jenkins/job/release-openesb-glassfish-addons/
OPENESB_IDE_DOWNLOAD_SITE=http://build2.open-esb.net:8080/jenkins/job/release-openesb-ide/


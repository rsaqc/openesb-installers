/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://www.netbeans.org/cddl.html or
 * http://www.netbeans.org/cddl.txt.
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://www.netbeans.org/cddl.txt. If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 *     "Portions Copyrighted [year] [name of copyright owner]"
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Portions Copyrighted 2011 IntegratedApps LLC
 */
package org.netbeans.installer.products.openesb;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.netbeans.installer.utils.applications.GlassFishUtils;
import org.netbeans.installer.utils.applications.NetBeansUtils;
import org.netbeans.installer.product.Registry;
import org.netbeans.installer.product.components.Product;
import org.netbeans.installer.utils.helper.Status;
import org.netbeans.installer.product.components.ProductConfigurationLogic;
import org.netbeans.installer.utils.SystemUtils;
import org.netbeans.installer.utils.exceptions.InitializationException;
import org.netbeans.installer.utils.exceptions.InstallationException;
import org.netbeans.installer.utils.exceptions.UninstallationException;
import org.netbeans.installer.utils.helper.Dependency;
import org.netbeans.installer.utils.helper.RemovalMode;
import org.netbeans.installer.utils.progress.Progress;
import org.netbeans.installer.wizard.Wizard;
import org.netbeans.installer.wizard.components.WizardComponent;
import org.netbeans.installer.utils.LogManager;

/**
 * @author Jeff Lin
 * @author Kir Sorokin, kir.sorokin@integrated-apps.com
 */
public class ConfigurationLogic extends ProductConfigurationLogic {

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Instance

    private List<WizardComponent> wizardComponents;

    public ConfigurationLogic(
            ) throws InitializationException {

        wizardComponents = Wizard.loadWizardComponents(
                WIZARD_COMPONENTS_URI,
                getClass().getClassLoader());
    }

    public void install(
            final Progress progress) throws InstallationException {

        final File openesbLocation = getProduct().getInstallationLocation();
        final File jbiInstaller = new File(openesbLocation, OPEN_ESB_CORE_INSTALLER);
        
        // get the list of suitable glassfish installations
        final List<Dependency> dependencies =
                getProduct().getDependencyByUid(GLASSFISH_UID);
        final List<Product> sources =
                Registry.getInstance().getProducts(dependencies.get(0));

        // pick the first one and integrate with it
        final File glassfishLocation = sources.get(0).getInstallationLocation();

        // resolve the dependency
        dependencies.get(0).setVersionResolved(sources.get(0).getVersion());

        try {
            progress.setDetail(getString("CL.install.stop.as")); // NOI18N
            GlassFishUtils.stopDefaultDomain(glassfishLocation);
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.stop.as"), // NOI18N
                    e);
        }

        // run the jbi core installer ////////////////////////////////////////////////
        try {
            File ideLocation = null;
            progress.setDetail(getString("CL.install.openesb.installer")); // NOI18N
            
            final List<Product> ides = Registry.getInstance().getProducts("openesb-ide");
            for (Product ide: ides) {
                if (ide.getStatus() == Status.INSTALLED) {
                    ideLocation = ide.getInstallationLocation();
                    break;
                }
            }

            String javaHome = "";
            if (ideLocation == null) {
                LogManager.log("ideLcation is null because openesb-ide is not installed");
                // Try getting the java home from GlassFishUtils instead in case NB is not
                // installed.
                if (GlassFishUtils.getJavaHome(glassfishLocation) != null) {
                    javaHome = GlassFishUtils.getJavaHome(glassfishLocation).getAbsolutePath();
                }
            } else {
                javaHome = NetBeansUtils.getJavaHome(ideLocation);
            }

            LogManager.log("The java home is " + javaHome);

            SystemUtils.executeCommand(
                    javaHome + File.separator + "bin" + File.separator + "java",
                    "-jar",
                    jbiInstaller.getAbsolutePath(),
                    glassfishLocation.getAbsolutePath(),
                    "install",
                    "overwrite");

        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.openesb.installer"), // NOI18N
                    e);
        } finally {

        }

        progress.setPercentage(Progress.COMPLETE);
    }

    public void uninstall(
            final Progress progress) {

        progress.setPercentage(Progress.COMPLETE);
    }

    public List<WizardComponent> getWizardComponents(
            ) {

        return wizardComponents;
    }

    @Override
    public boolean registerInSystem(
            ) {

        return false;
    }

    @Override
    public RemovalMode getRemovalMode(
            ) {

        return RemovalMode.LIST;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Constants

    public static final String WIZARD_COMPONENTS_URI =
            "resource:" + // NOI18N
            "org/netbeans/installer/products/openesb/wizard.xml"; // NOI18N

    private static final String GLASSFISH_UID =
            "glassfish"; // NOI18N

    private static final String OPEN_ESB_CORE_INSTALLER =
            "openesb-core.jar"; // NOI18N
}

/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://www.netbeans.org/cddl.html or
 * http://www.netbeans.org/cddl.txt.
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://www.netbeans.org/cddl.txt. If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 *     "Portions Copyrighted [year] [name of copyright owner]"
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Portions Copyrighted 2011 IntegratedApps LLC
 */
package org.netbeans.installer.products.jbicomponents;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.netbeans.installer.utils.applications.GlassFishUtils;
import org.netbeans.installer.product.Registry;
import org.netbeans.installer.product.components.Product;
import org.netbeans.installer.product.components.ProductConfigurationLogic;
import org.netbeans.installer.utils.SystemUtils;
import org.netbeans.installer.utils.exceptions.InitializationException;
import org.netbeans.installer.utils.exceptions.InstallationException;
import org.netbeans.installer.utils.helper.Dependency;
import org.netbeans.installer.utils.helper.RemovalMode;
import org.netbeans.installer.utils.progress.Progress;
import org.netbeans.installer.wizard.Wizard;
import org.netbeans.installer.wizard.components.WizardComponent;
import org.netbeans.installer.utils.LogManager;
import org.netbeans.installer.utils.exceptions.XMLException;
import org.netbeans.installer.utils.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * @author Jeff Lin
 * @author Kir Sorokin, kir.sorokin@integrated-apps.com
 */
public class ConfigurationLogic extends ProductConfigurationLogic {

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Instance

    private List<WizardComponent> wizardComponents;

    public ConfigurationLogic(
            ) throws InitializationException {

        wizardComponents = Wizard.loadWizardComponents(
                WIZARD_COMPONENTS_URI,
                getClass().getClassLoader());
    }

    public List getWizardComponents(
            ) {

        return wizardComponents;
    }

    public void install(
            final Progress progress) throws InstallationException {

        LogManager.log("In OpenESB Components install logic");
        final File jbicompsLocation = getProduct().getInstallationLocation();


        // get the list of suitable glassfish installations
        final List<Dependency> dependencies =
                getProduct().getDependencyByUid(GLASSFISH_UID);
        final List<Product> sources =
                Registry.getInstance().getProducts(dependencies.get(0));

        // pick the first one and integrate with it
        final File glassfishLocation = sources.get(0).getInstallationLocation();

        // resolve the dependency
        dependencies.get(0).setVersionResolved(sources.get(0).getVersion());

        // start the default domain //////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.start.as")); // NOI18N

            GlassFishUtils.startDefaultDomain(glassfishLocation);
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.start.as"),  // NOI18N
                    e);
        }

        final File asadmin = GlassFishUtils.getAsadmin(glassfishLocation);
        final File asant = GlassFishUtils.getAsant(glassfishLocation);
        final String username  = System.getProperty(USERNAME_PROPERTY);
        final String password  = System.getProperty(PASSWORD_PROPERTY);
        System.setProperty(USERNAME_PROPERTY, "");
        System.setProperty(PASSWORD_PROPERTY, "");
        LogManager.log("The username is " + username);
        //LogManager.log("The password is " + password);


        File passwordFile = null;
        try {
            passwordFile = GlassFishUtils.createPasswordFile(password, glassfishLocation);
        } catch (IOException e) {
            throw new InstallationException("Couldnot create the password file", e);
        }
        // Starting derby database  ////////////////////////////////////////////////
        try {
            //progress.setDetail(getString("CL.install.jbi.components")); // NOI18N
            progress.setDetail(getString("CL.start.db")); // NOI18N

            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "start-database");
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.start.database.error"),  // NOI18N
                    e);
        }

        // configuring glassfish domain.xml  ////////////////////////////////////////////////
        try {
            
            final String adminPort = "" + GlassFishUtils.getAdminPort(glassfishLocation, "domain1");
            progress.setDetail(getString("CL.create.jvm.options") + "MaxPermSize=128m"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jvm-options",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "\\-XX\\:MaxPermSize=128m");
            progress.setDetail(getString("CL.create.jvm.options") + "PermSize=128m"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jvm-options",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "\\-XX\\:PermSize=128m");
            progress.setDetail(getString("CL.create.jvm.options") + "disableCaptureStackTrace=false"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jvm-options",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "\\-Dcom.sun.xml.ws.fault.SOAPFaultBuilder.disableCaptureStackTrace=false");
            
            progress.setDetail(getString("CL.create.jvm.options") + "xml.catalog.ignoreMissing=true"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jvm-options",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "\\-Dxml.catalog.ignoreMissing=true");
            
            progress.setDetail(getString("CL.delete.jdbc.connection.pool") + "DerbyPool"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "delete-jdbc-connection-pool",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "--cascade",
                    "DerbyPool");
            progress.setDetail(getString("CL.create.jdbc.connection.pool") + "DerbyPool"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jdbc-connection-pool",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "--allownoncomponentcallers=false",
                    "--idletimeout", "300",
                    "--maxpoolsize", "32",
                    "--maxwait", "60000",
                    "--poolresize", "2",
                    "--steadypoolsize", "8",
                    "--validationmethod", "auto-commit",
                    "--datasourceclassname", "org.apache.derby.jdbc.ClientDataSource",
                    "--restype", "javax.sql.DataSource",
                    "--property", "PortNumber=1527:Password=app:User=app:serverName=localhost:DatabaseName=sample:connectionAttributes=;create\\=true",
                    "DerbyPool");

            progress.setDetail(getString("CL.create.jdbc.resource") + "jdbc/__defaultDS"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jdbc-resource",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "--connectionpoolid", "DerbyPool",
                    "jdbc/__defaultDS");
            progress.setDetail(getString("CL.create.jdbc.resource") + "jdbc/__default"); // NOI18N
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "create-jdbc-resource",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "--port",
                    adminPort,
                    "--connectionpoolid", "DerbyPool",
                    "jdbc/__default");

        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.cofnigure.glassfish.domain.error"),  // NOI18N
                    e);
        } catch (XMLException e) {
            throw new InstallationException(
                    getString("CL.cofnigure.glassfish.domain.error"),  // NOI18N
                    e);
        }

        // Install JBI components  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        try {
            
            progress.setDetail(getString("CL.upgrade.jbi.component") + "sun-http-binding"); // NOI18N
            File http = new File(jbicompsLocation, "httpbc.jar");
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "upgrade-jbi-component",
                    "--upgradefile",
                    http.getAbsolutePath(),
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    "sun-http-binding");
                    
           // configure the OutboundThread with a value defined in jbi.xml
           // ignoring any errors
           String outboundThread = getOutboundThreadFromJBIXml(http);
           if (outboundThread != null) {
               // after an upgrade, httpbc has to be in a shutdown stage
               // so we need to start it to be able to configure
               progress.setDetail(getString("CL.start.jbi.component") + // NOI18N
                       "sun-http-binding"); // NOI18N
               SystemUtils.executeCommand(
                       asadmin.getParentFile(),
                       asadmin.getAbsolutePath(),
                       "start-jbi-component",
                       "--user",
                       username,
                       "--passwordfile",
                       passwordFile.getAbsolutePath(),
                       "sun-http-binding");

               // list status
               SystemUtils.executeCommand(
                       asadmin.getParentFile(),
                       asadmin.getAbsolutePath(),
                       "show-jbi-binding-component",
                       "--user",
                       username,
                       "--passwordfile",
                       passwordFile.getAbsolutePath(),
                       "sun-http-binding");

               // configure the OutboundThreads
               progress.setDetail(getString("CL.configure.jbi.component") + // NOI18N
                       "sun-http-binding"); // NOI18N
               SystemUtils.executeCommand(
                       asadmin.getParentFile(),
                       asadmin.getAbsolutePath(),
                       "set-jbi-component-configuration",
                       "--user",
                       username,
                       "--passwordfile",
                       passwordFile.getAbsolutePath(),
                       "--component",
                       "sun-http-binding",
                       OUTBOUNDTHREADS_PROPERTY + "=" + outboundThread);

               // shut it down now to keep previous state
               progress.setDetail(getString("CL.shutdown.jbi.component") + // NOI18N
                       "sun-http-binding"); // NOI18N
               SystemUtils.executeCommand(
                       asadmin.getParentFile(),
                       asadmin.getAbsolutePath(),
                       "shut-down-jbi-component",
                       "--user",
                       username,
                       "--passwordfile",
                       passwordFile.getAbsolutePath(),
                       "sun-http-binding");

           } else {
               LogManager.log("OutboundThreads property cannot be found.");
           }

            progress.setDetail(getString("CL.install.jbi.component") + "sun-encoder-library"); // NOI18N
            File sl = new File(jbicompsLocation, "encoderlib.jar");
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "install-jbi-shared-library",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    sl.getAbsolutePath());

            progress.setDetail(getString("CL.install.jbi.component") + "sun-wsdl-ext-library"); // NOI18N
            sl = new File(jbicompsLocation, "wsdlextlib.jar");
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "install-jbi-shared-library",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    sl.getAbsolutePath());
            /*
            progress.setDetail(getString("CL.install.jbi.component") + "sun-saxon-library"); // NOI18N
            sl = new File(jbicompsLocation, "saxonlib.jar");
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "install-jbi-shared-library",
                    "--user",
                    username,
                    "--passwordfile",
                    passwordFile.getAbsolutePath(),
                    sl.getAbsolutePath());
            */

            // installing all available engines and binding components
            for (int i = 0; i < componentJars.length; i++) {

                progress.setDetail(getString("CL.install.jbi.component") + componentNames[i]); // NOI18N

                File component = new File(jbicompsLocation, componentJars[i]);
                SystemUtils.executeCommand(
                        asadmin.getParentFile(),
                        asadmin.getAbsolutePath(),
                        "install-jbi-component",
                        "--user",
                        username,
                        "--passwordfile",
                        passwordFile.getAbsolutePath(),
                        component.getAbsolutePath());

            }

        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.jbi.components.error"),  // NOI18N
                    e);
        }
        
        // install the openesb glassfish addons installer
        // zip file is unzipped into addons/openesb_addons folder
        ///////////////////////////////////////////////////
        try {
            final String adminPort = "" + GlassFishUtils.getAdminPort(glassfishLocation, "domain1");
            FileUtils.unzip(
                    new File(jbicompsLocation, "openesb-glassfish-addons.zip"),
                    new File(glassfishLocation, "addons"));

            File fileLocation = new File(glassfishLocation, "addons/openesb_addons/install.xml");
            SystemUtils.executeCommand(
                    asant.getParentFile(),
                    asant.getAbsolutePath(),
                    "-f",
                    fileLocation.getAbsolutePath(),
                    "-Dcaps.domain.name=" + "domain1",
                    "-Dcaps.admin.user=" + username,
                    "-Dcaps.password.file=" + passwordFile.getAbsolutePath(),
                    "-Dcaps.admin.port=" + adminPort);

        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.openesb.glassfish.addons.error"),  // NOI18N
                    e);
        } catch (XMLException e) {
            throw new InstallationException(
                    getString("CL.install.openesb.glassfish.addons.error"),  // NOI18N
                    e);
        }

        // install the saxonlib.zip by unzipping into <GF> folder
        ///////////////////////////////////////////////////
        try {
            File addOnsJbi = new File(jbicompsLocation, "jbi");
            File gfJbi = new File(glassfishLocation, "jbi");
            if ((addOnsJbi != null) && (addOnsJbi.exists())) {
                FileUtils.copyFile(addOnsJbi, gfJbi, true);
                FileUtils.deleteFile(addOnsJbi, true);
            }
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.openesb.glassfish.addons.error"),  // NOI18N
                    e);
        }

        passwordFile.delete();
        
        // stop the default domain //////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.stop.as")); // NOI18N

            GlassFishUtils.stopDefaultDomain(glassfishLocation);
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.stop.as"),  // NOI18N
                    e);
        }

        // stop derby database  ////////////////////////////////////////////////
        try {
            SystemUtils.executeCommand(
                    asadmin.getParentFile(),
                    asadmin.getAbsolutePath(),
                    "stop-database");
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.stop.database.error"),  // NOI18N
                    e);
        }

        progress.setPercentage(Progress.COMPLETE);
    }

    public void uninstall(
            final Progress progress) {

        progress.setPercentage(Progress.COMPLETE);
    }

    @Override
    public boolean registerInSystem(
            ) {

        return false;
    }

    @Override
    public RemovalMode getRemovalMode(
            ) {

        return RemovalMode.LIST;
    }

    // Private -------------------------------------------------------------------------------------
    private String getOutboundThreadFromJBIXml(
            final File httpBCFile) {

        if (httpBCFile == null) {
            return null;
        }

        String outboundThread = null;
        try {
            final JarFile jarFile = new JarFile(httpBCFile.getAbsolutePath());
            JarEntry jbiXML = jarFile.getJarEntry("META-INF/jbi.xml");
            if (jbiXML == null) {
                jbiXML = jarFile.getJarEntry("meta-inf/jbi.xml");
            }

            if (jbiXML != null) {
                final InputStream inputStream = jarFile.getInputStream(jbiXML);
                if (inputStream != null) {
                    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    factory.setValidating(false);
                    factory.setNamespaceAware(true);

                    final DocumentBuilder documentBuilder = factory.newDocumentBuilder();
                    final Document suJbiDocument = documentBuilder.parse(inputStream);
                    final NodeList idElements = suJbiDocument.getElementsByTagName("config:Configuration"); //NOI18N
                    if ((idElements != null) && (idElements.getLength() > 0)) {
                        final Node fstNode = idElements.item(0);
                        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                            final Element fstElmnt = (Element) fstNode;

                            final NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("config:Property"); // NOI18N
                            if ((fstNmElmntLst != null) && (fstNmElmntLst.getLength() > 0)) {
                                for (int t = 0; t < fstNmElmntLst.getLength(); t++) {
                                    final Node fstDomainNode = fstNmElmntLst.item(t);
                                    if (fstDomainNode.getNodeType() == Node.ELEMENT_NODE) {
                                        final Element element = (Element) fstDomainNode;
                                        final String name = element.getAttribute("name"); // NOI18N
                                        if ((name != null) && (name.equals(OUTBOUNDTHREADS_PROPERTY))) {
                                            final String defaultValue = element.getAttribute("defaultValue"); // NOI18N
                                            if (defaultValue != null) {
                                                outboundThread = defaultValue;
                                                break;
                                            }
                                        } else {
                                            LogManager.log("Could not find " + OUTBOUNDTHREADS_PROPERTY + " tag.");
                                        }
                                    }
                                }
                            } else {
                                LogManager.log("Could not find config:Property tag.");
                            }
                        }
                    } else {
                        LogManager.log("Could not find config:Configuration tag.");
                    }
                    // close streams
                    inputStream.close();
                    jarFile.close();
                } else {
                    LogManager.log("Invalid input stream for sun-http-binding.jar");
                }
            } else {
                LogManager.log(
                        "Could not find meta-inf/jbi.xml from " + httpBCFile.getAbsolutePath());
            }
        } catch (Exception ex) {
            LogManager.logIndent(ex.getMessage());

        }

        return outboundThread;
    } 

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Constants

    private static final String GLASSFISH_UID =
            "glassfish"; // NOI18N

    private static final String WIZARD_COMPONENTS_URI =
            "resource:" + // NOI18N
            "org/netbeans/installer/products/jbicomponents/wizard.xml"; // NOI18N

    private static final String USERNAME_PROPERTY = "username";
    private static final String PASSWORD_PROPERTY = "password";
    private static final String OUTBOUNDTHREADS_PROPERTY = "OutboundThreads";

    private static final String componentJars[] = {
        "bpelse.jar",
        "databasebc.jar",
//        "edmse.jar",
        "filebc.jar",
        "ftpbc.jar",
        "jmsbc.jar",
        "ldapbc.jar",
        "schedulerbc.jar",
        "xsltse.jar",
        "emailbc.jar",
        "pojose.jar",
        "restbc.jar",
        "iepse.jar",
        "worklistmanagerse.jar",
        "etlse.jar"
        };

    private static final String componentNames[] = {
        "sun-bpel-engine",
        "sun-database-binding",
//        "sun-edm-engine",
        "sun-file-binding",
        "sun-ftp-binding",
        "sun-jms-binding",
        "sun-ldap-binding",
        "sun-scheduler-binding",
        "sun-xslt-engine",
        "sun-email-binding",
        "sun-pojo-engine",
        "sun-rest-binding",
        "sun-iep-engine",
        "sun-wlm-engine",
        "sun-etl-engine"
        };

}

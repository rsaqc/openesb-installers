package org.netbeans.installer.openesb;

import java.io.File;
import org.netbeans.installer.utils.DateUtils;
import org.netbeans.installer.utils.LogManager;

public class OpenESBUserDir {

    public static String getUserDir() {
            String prefix = System.getProperty("user.home") + File.separator + ".openesb-installer-";
            String hostname = "localhost";
            try {
                hostname = java.net.InetAddress.getLocalHost().getHostName();
            } catch (Exception e) {
                LogManager.log("... could not get host name, using localhost instead");
            }
            return new File(prefix + hostname + "_" + DateUtils.getTimestamp()).getAbsolutePath();
    }
}
/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://www.netbeans.org/cddl.html or
 * http://www.netbeans.org/cddl.txt.
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://www.netbeans.org/cddl.txt. If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 *     "Portions Copyrighted [year] [name of copyright owner]"
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * Portions Copyrighted 2011 IntegratedApps LLC
 */
package org.netbeans.installer.infra.build.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author Kir Sorokin, kir.sorokin@integrated-apps.com
 */
public class BuildNumberAndVersionForOpenEsbComponents extends Task {

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Instance

    /**
     * The input file.
     */
    private File file;

    /**
     * Parses the OpenESB Components build number and version.
     *
     * @throws BuildException
     *      If an whichever error occurs while parsing the build number.
     */
    @Override
    public void execute(
            ) throws BuildException {

        FileInputStream input = null;
        try {
            final Properties properties = new Properties();

            input = new FileInputStream(file);
            properties.load(input);

            getProject().setProperty("openesb-components.version", ""
                    +  properties.getProperty("major.version") + "."
                    +  properties.getProperty("minor.version") + "."
                    +  properties.getProperty("micro.version") + "."
                    +  properties.getProperty("build.number") + "."
                    +  properties.getProperty("build.timestamp"));
            getProject().setProperty("openesb-components.build.number", ""
                    +  properties.getProperty("build.number"));

        } catch (IOException e) {
            throw new BuildException(e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new BuildException(e);
                }
            }
        }
    }

    /**
     * Setter for the {@link #file} property.
     *
     * @param path The value of the {@link #file} property.
     */
    public void setFile(
            final String path) {

        this.file = new File(path);
    }

}
